package fr.lavie.alexis.heylp.backend.dao;

import fr.lavie.alexis.heylp.backend.model.User;

public interface UserDao extends AbstractDao<User> {

    long login(String username, String password);
}
