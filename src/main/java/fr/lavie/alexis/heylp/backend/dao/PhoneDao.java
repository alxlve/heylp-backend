package fr.lavie.alexis.heylp.backend.dao;

import fr.lavie.alexis.heylp.backend.model.Phone;

public interface PhoneDao extends AbstractDao<Phone> {
}
