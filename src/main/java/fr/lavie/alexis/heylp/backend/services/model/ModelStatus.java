package fr.lavie.alexis.heylp.backend.services.model;

public enum ModelStatus {
    OK,
    CONFLICT,
    NO_CONTENT,
    NOT_FOUND
}
