package fr.lavie.alexis.heylp.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceCategory;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceStatus;
import fr.lavie.alexis.heylp.backend.services.model.APIModel;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement
@Entity
public class Service {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @JsonIgnore
    @ManyToOne
    private User owner;
    @JsonIgnore
    @ManyToOne
    private User suscriber;
    private ServiceCategory category;
    private String title;
    @Column(columnDefinition = "TEXT")
    private String description;
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Picture picture;
    @OneToOne(targetEntity = Address.class, cascade = CascadeType.ALL, orphanRemoval = true)
    private Address address;
    private float price;
    private Date startDate;
    private Date endDate;
    private Date creationDate;
    @Enumerated(EnumType.ORDINAL)
    private ServiceStatus status;
    private Float ownerNote;
    private Float suscriberNote;

    public Service(ServiceCategory category, String title, String description, float price, Date startDate, Date endDate, Date creationDate) {
        this();

        this.category = category;
        this.title = title;
        this.description = description;
        this.price = price;
        this.startDate = startDate;
        this.endDate = endDate;
        this.creationDate = creationDate;
        this.status = ServiceStatus.AVAILABLE;
    }

    public Service() {
        super();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty("owner")
    public long getOwnerId() {
        if (owner != null) {
            return owner.getId();
        }
        return 0;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    @JsonProperty("suscriber")
    public Long getSuscriberId() {
        if (suscriber != null) {
            return suscriber.getId();
        }

        return null;
    }

    @JsonProperty("suscriber")
    public void setSuscriberId(Long idSuscriber) {
        User user = fr.lavie.alexis.heylp.backend.services.Application.getInstance().getModel().getUser(idSuscriber);
        user.addAcceptedService(this);
    }

    public User getSuscriber() {
        return suscriber;
    }

    public void setSuscriber(User suscriber) {
        this.suscriber = suscriber;
    }

    public ServiceCategory getCategory() {
        return category;
    }

    public void setCategory(ServiceCategory category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        address.setServiceOwner(this);
        this.address = address;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public ServiceStatus getStatus() {
        return status;
    }

    public void setStatus(ServiceStatus status) {
        this.status = status;
    }

    public Float getOwnerNote() {
        return ownerNote;
    }

    public void setOwnerNote(Float ownerNote) {
        this.ownerNote = ownerNote;
    }

    public Float getSuscriberNote() {
        return suscriberNote;
    }

    public void setSuscriberNote(Float suscriberNote) {
        this.suscriberNote = suscriberNote;
    }
}
