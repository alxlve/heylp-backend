package fr.lavie.alexis.heylp.backend.model.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Gender {

    @JsonProperty("Male")
    MALE(1),
    @JsonProperty("Female")
    FEMALE(2);

    private Integer value;

    Gender(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }
}
