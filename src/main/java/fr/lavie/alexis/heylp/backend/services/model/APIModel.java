package fr.lavie.alexis.heylp.backend.services.model;

import fr.lavie.alexis.heylp.backend.model.Phone;
import fr.lavie.alexis.heylp.backend.model.Picture;
import fr.lavie.alexis.heylp.backend.model.Service;
import fr.lavie.alexis.heylp.backend.model.User;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceCategory;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceStatus;

import javax.ws.rs.core.Response;
import java.util.Collection;

public interface APIModel {

    String hello(String string);

    long login(String username, String password);

    User getUser(long idUser);

    Collection<User> getAllUsers();

    long addUser(User user);

    Collection<Long> addUsers(User... users);

    ModelStatus saveUser(User User);

    Collection<ModelStatus> saveUsers(User... users);

    ModelStatus deleteUser(User user);

    Collection<ModelStatus> deleteUsers(User... users);

    Picture getUserPicture(long idUser);

    long addUserPicture(User user, Picture picture);

    Phone getUserPhone(User user, long idPhone);

    Collection<Phone> getAllUserPhones(User user);

    long addUserPhone(User user, Phone phone);

    Collection<Long> addUserPhones(User user, Phone... phones);

    ModelStatus saveUserPhone(User user, Phone phone);

    Collection<ModelStatus> saveUserPhones(User user, Phone... phones);

    ModelStatus deleteUserPhone(User user, Phone phone);

    Collection<ModelStatus> deleteUserPhones(User user, Phone... phones);

    Service getUserProposedService(User user, long idService);

    Collection<Service> getAllUserProposedServices(long idUser);

    Collection<Service> getAllUserSuscribedServices(long idUser, ServiceStatus serviceStatus);

    long addUserProposedService(User user, Service service);

    ModelStatus saveUserProposedService(User user, Service service);

    Picture getServicePicture(User user, long idService);

    long addServicePicture(Service service, Picture picture);

    Collection<Service> getAllServicesByStatus(ServiceStatus serviceStatus);

    Collection<Service> getAllServicesByStatusAndCategory(ServiceStatus serviceStatus, ServiceCategory serviceCategory);
}
