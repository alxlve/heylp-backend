package fr.lavie.alexis.heylp.backend.services.rest;

import fr.lavie.alexis.heylp.backend.model.Service;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceCategory;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceStatus;
import fr.lavie.alexis.heylp.backend.services.model.APIModel;
import fr.lavie.alexis.heylp.backend.services.rest.helper.MediaTypeUTF8;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import java.util.Collection;
import java.util.Locale;

@Path("_service")
public class ServiceREST {

    private static APIModel model;

    @Context
    UriInfo uriInfo;

    public ServiceREST() {
        super();

        model = fr.lavie.alexis.heylp.backend.services.Application.getInstance().getModel();
    }

    @GET
    @Path("available")
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Collection<Service> getAllAvailableServices() {
        return model.getAllServicesByStatus(ServiceStatus.AVAILABLE);
    }

    @POST
    @Path("available")
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Collection<Service> getAllAvailableServices(String jsonString) {
        JSONObject jsonObject = new JSONObject(jsonString);
        ServiceCategory serviceCategory = ServiceCategory.valueOf(jsonObject.getString("category").toUpperCase(Locale.ENGLISH));
        return model.getAllServicesByStatusAndCategory(ServiceStatus.AVAILABLE, serviceCategory);
    }
}
