package fr.lavie.alexis.heylp.backend.services.model;

import fr.lavie.alexis.heylp.backend.dao.AddressDao;
import fr.lavie.alexis.heylp.backend.dao.PhoneDao;
import fr.lavie.alexis.heylp.backend.dao.ServiceDao;
import fr.lavie.alexis.heylp.backend.dao.UserDao;
import fr.lavie.alexis.heylp.backend.model.Phone;
import fr.lavie.alexis.heylp.backend.model.Picture;
import fr.lavie.alexis.heylp.backend.model.Service;
import fr.lavie.alexis.heylp.backend.model.User;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceCategory;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceStatus;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;

@org.springframework.stereotype.Service
@Transactional
public class SpringAPIModel implements APIModel {

    private AddressDao addressDao;
    private PhoneDao phoneDao;
    private ServiceDao serviceDao;
    private UserDao userDao;

    public SpringAPIModel(AddressDao addressDao, PhoneDao phoneDao, ServiceDao serviceDao, UserDao userDao) {
        this.addressDao = addressDao;
        this.phoneDao = phoneDao;
        this.serviceDao = serviceDao;
        this.userDao = userDao;
    }

    @Override
    public String hello(String string) {
        return "Hello " + string + ", how are you?\n";
    }

    @Override
    public long login(String username, String password) {
        return userDao.login(username, password);
    }

    @Override
    public User getUser(long idUser) {
        return userDao.get(idUser);
    }

    @Override
    public Collection<User> getAllUsers() {
        return userDao.getAll();
    }

    @Override
    public long addUser(User user) {userDao.add(user);
        return user.getId();
    }

    @Override
    public Collection<Long> addUsers(User... users) {
        Collection<Long> collection = new ArrayList<Long>();

        for (User user : users) {
            collection.add(addUser(user));
        }

        return collection;
    }

    @Override
    public ModelStatus saveUser(User user) {
        userDao.save(user);

        return ModelStatus.OK;
    }

    @Override
    public Collection<ModelStatus> saveUsers(User... users) {
        Collection<ModelStatus> collection = new ArrayList<ModelStatus>();

        for (User user : users) {
            collection.add(saveUser(user));
        }

        return collection;
    }

    @Override
    public ModelStatus deleteUser(User user) {
        userDao.delete(user);

        return ModelStatus.OK;
    }

    @Override
    public Collection<ModelStatus> deleteUsers(User... users) {
        Collection<ModelStatus> collection = new ArrayList<ModelStatus>();

        for (User user : users) {
            collection.add(deleteUser(user));
        }

        return collection;
    }

    @Override
    public Picture getUserPicture(long idUser) {
        return getUser(idUser).getPicture();
    }

    @Override
    public long addUserPicture(User user, Picture picture) {
        user.setPicture(picture);
        userDao.save(user);

        return picture.getId();
    }

    @Override
    public Phone getUserPhone(User user, long idPhone) {
        return phoneDao.get(idPhone);
    }

    @Override
    public Collection<Phone> getAllUserPhones(User user) {
        return user.getPhones();
    }

    @Override
    public long addUserPhone(User user, Phone phone) {
        user.addPhone(phone);
        phoneDao.add(phone);
        userDao.save(user);

        return phone.getId();
    }

    @Override
    public Collection<Long> addUserPhones(User user, Phone... phones) {
        Collection<Long> collection = new ArrayList<Long>();

        for (Phone phone : phones) {
            collection.add(addUserPhone(user, phone));
        }

        return collection;
    }

    @Override
    public ModelStatus saveUserPhone(User user, Phone phone) {
        phoneDao.save(phone);

        return ModelStatus.OK;
    }

    @Override
    public Collection<ModelStatus> saveUserPhones(User user, Phone... phones) {
        Collection<ModelStatus> collection = new ArrayList<ModelStatus>();

        for (Phone phone : phones) {
            collection.add(saveUserPhone(user, phone));
        }

        return collection;
    }

    @Override
    public ModelStatus deleteUserPhone(User user, Phone phone) {
        phoneDao.save(phone);

        return ModelStatus.OK;
    }

    @Override
    public Collection<ModelStatus> deleteUserPhones(User user, Phone... phones) {
        Collection<ModelStatus> collection = new ArrayList<ModelStatus>();

        for (Phone phone : phones) {
            collection.add(deleteUserPhone(user, phone));
        }

        return collection;
    }

    @Override
    public Service getUserProposedService(User user, long idService) {
        return serviceDao.get(idService);
    }

    @Override
    public Collection<Service> getAllUserProposedServices(long idUser) {
        return serviceDao.getAllByOwner(getUser(idUser));
    }

    @Override
    public Collection<Service> getAllUserSuscribedServices(long idUser, ServiceStatus serviceStatus) {
        return serviceDao.getAllBySuscriberAndStatus(getUser(idUser), serviceStatus);
    }

    @Override
    public long addUserProposedService(User user, Service service) {
        user.addProposedService(service);
        serviceDao.add(service);
        userDao.save(user);

        return service.getId();
    }

    @Override
    public ModelStatus saveUserProposedService(User user, Service service) {
        Picture picture = getServicePicture(user, service.getId());
        service.setPicture(picture);
        serviceDao.save(service);
        userDao.save(service.getSuscriber());

        return ModelStatus.OK;
    }

    @Override
    public Picture getServicePicture(User user, long idService) {
        return serviceDao.get(idService).getPicture();
    }

    @Override
    public long addServicePicture(Service service, Picture picture) {
        service.setPicture(picture);
        serviceDao.save(service);

        return picture.getId();
    }

    @Override
    public Collection<Service> getAllServicesByStatus(ServiceStatus serviceStatus) {
        return serviceDao.getAllByStatus(serviceStatus);
    }

    @Override
    public Collection<Service> getAllServicesByStatusAndCategory(ServiceStatus serviceStatus, ServiceCategory serviceCategory) {
        return serviceDao.getAllByStatusAndCategory(serviceStatus, serviceCategory);
    }
}
