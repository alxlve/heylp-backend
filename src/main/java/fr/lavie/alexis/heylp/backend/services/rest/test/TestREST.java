package fr.lavie.alexis.heylp.backend.services.rest.test;

import fr.lavie.alexis.heylp.backend.services.rest.helper.MediaTypeUTF8;
import fr.lavie.alexis.heylp.backend.services.model.APIModel;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("test")
public class TestREST {

    private static APIModel model;

    public TestREST() {
        super();

        model = fr.lavie.alexis.heylp.backend.services.Application.getInstance().getModel();
    }

    @GET
    @Path("hello/{text}")
    @Produces(MediaType.TEXT_HTML)
    public Response hello(@PathParam("text") String text) {
        String result = model.hello(text);

        return Response.status(200).entity(result).build();
    }

    @POST
    @Path("hello")
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Response helloPOST(String text) {
        String result = model.hello(text);

        return Response.status(200).entity(result).build();
    }

    @GET
    @Path("repeat")
    @Produces(MediaType.TEXT_HTML)
    public Response repeatText(@QueryParam("text") String text) {
        return Response.status(200).entity(text).build();
    }

    @GET
    @Path("repeat/{text}")
    @Produces(MediaType.TEXT_HTML)
    public Response repeatURL(@PathParam("text") String text) {
        return Response.status(200).entity(text).build();
    }

    @GET
    @Path("verify")
    @Produces(MediaType.TEXT_PLAIN)
    public Response verify() {
        String result = "TestREST has successfully started.";

        return Response.status(200).entity(result).build();
    }
}
