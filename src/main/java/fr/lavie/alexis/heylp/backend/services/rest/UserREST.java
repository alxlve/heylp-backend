package fr.lavie.alexis.heylp.backend.services.rest;

import fr.lavie.alexis.heylp.backend.model.Phone;
import fr.lavie.alexis.heylp.backend.model.Picture;
import fr.lavie.alexis.heylp.backend.model.Service;
import fr.lavie.alexis.heylp.backend.model.User;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceStatus;
import fr.lavie.alexis.heylp.backend.services.model.APIModel;
import fr.lavie.alexis.heylp.backend.services.rest.helper.MediaTypeUTF8;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.util.Collection;

@Path("user")
public class UserREST {

    private static APIModel model;

    @Context
    UriInfo uriInfo;

    public UserREST() {
        super();

        model = fr.lavie.alexis.heylp.backend.services.Application.getInstance().getModel();
    }

    @GET
    @Path("{idUser}")
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public User getUser(@PathParam("idUser") long idUser) {
        return model.getUser(idUser);
    }

    @GET
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Collection<User> getAllUsers() {
        return model.getAllUsers();
    }

    @POST
    @Consumes(MediaTypeUTF8.APPLICATION_JSON)
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Response addUser(User user) {
        long idUser = model.addUser(user);
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(Long.toString(idUser));

        return Response.created(uriBuilder.build()).entity(idUser).build();
    }

    @PUT
    @Path("{idUser}")
    @Consumes(MediaTypeUTF8.APPLICATION_JSON)
    public Response saveUser(@PathParam("idUser") long idUser, User user) {
        model.saveUser(user);

        return Response.ok().build();
    }

    @DELETE
    @Path("{idUser}")
    public Response deleteUser(@PathParam("idUser") long idUser) {
        model.deleteUser(getUser(idUser));

        return Response.ok().build();
    }

    @GET
    @Path("{idUser}/picture")
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Picture getUserPicture(@PathParam("idUser") long idUser) {
        return model.getUserPicture(idUser);
    }

    @POST
    @Path("{idUser}/picture")
    @Consumes(MediaTypeUTF8.APPLICATION_JSON)
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Response addUserPicture(@PathParam("idUser") long idUser, Picture picture) {
        long idPicture = model.addUserPicture(model.getUser(idUser), picture);
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(Long.toString(idPicture));

        return Response.created(uriBuilder.build()).entity(idPicture).build();
    }

    @GET
    @Path("{idUser}/phone/{idPhone}")
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Phone getUserPhone(@PathParam("idUser") long idUser, @PathParam("idPhone") long idPhone) {
        return model.getUserPhone(model.getUser(idUser), idPhone);
    }

    @GET
    @Path("{idUser}/phone")
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Collection<Phone> getAllUserPhones(@PathParam("idUser") long idUser) {
        return model.getAllUserPhones(model.getUser(idUser));
    }

    @POST
    @Path("{idUser}/phone")
    @Consumes(MediaTypeUTF8.APPLICATION_JSON)
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Response addUserPhone(@PathParam("idUser") long idUser, Phone phone) {
        long idPhone = model.addUserPhone(model.getUser(idUser), phone);
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(Long.toString(idPhone));

        return Response.created(uriBuilder.build()).entity(idPhone).build();
    }

    @PUT
    @Path("{idUser}/phone/{idPhone}")
    @Consumes(MediaTypeUTF8.APPLICATION_JSON)
    public Response saveUserPhone(@PathParam("idUser") long idUser, @PathParam("idPhone") long idPhone, Phone phone) {
        // TODO

        return Response.ok().build();
    }

    @DELETE
    @Path("{idUser}/phone/{idPhone}")
    public Response deleteUserPhone(@PathParam("idUser") long idUser, @PathParam("idPhone") long idPhone) {
        // TODO

        return Response.ok().build();
    }

    @GET
    @Path("{idUser}/service/{idService}")
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Service getUserProposedService(@PathParam("idUser") long idUser, @PathParam("idService") long idService) {
        return model.getUserProposedService(model.getUser(idUser), idService);
    }

    @GET
    @Path("{idUser}/service")
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Collection<Service> getUserAllProposedServices(@PathParam("idUser") long idUser) {
        return model.getAllUserProposedServices(idUser);
    }

    @GET
    @Path("{idUser}/service-affected")
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Collection<Service> getUserAllAffectedServices(@PathParam("idUser") long idUser) {
        return model.getAllUserSuscribedServices(idUser, ServiceStatus.AFFECTED);
    }

    @GET
    @Path("{idUser}/service-accomplished")
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Collection<Service> getUserAllAccomplishedServices(@PathParam("idUser") long idUser) {
        return model.getAllUserSuscribedServices(idUser, ServiceStatus.ACCOMPLISHED);
    }

    @POST
    @Path("{idUser}/service")
    @Consumes(MediaTypeUTF8.APPLICATION_JSON)
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Response addUserProposedServices(@PathParam("idUser") long idUser, Service service) {
        long idService = model.addUserProposedService(model.getUser(idUser), service);
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(Long.toString(idService));

        return Response.created(uriBuilder.build()).entity(idService).build();
    }

    @PUT
    @Path("{idUser}/service/{idService}")
    @Consumes(MediaTypeUTF8.APPLICATION_JSON)
    public Response saveUserProposedServices(@PathParam("idUser") long idUser, @PathParam("idService") long idService, Service service) {
        service.setId(idService);
        User user = model.getUser(idUser);
        service.setOwner(user);
        model.saveUserProposedService(model.getUser(idUser), service);

        return Response.ok().build();
    }

    @DELETE
    @Path("{idUser}/service/{idService}")
    public Response deleteUserProposedServices(@PathParam("idUser") long idUser, @PathParam("idService") long idService) {

        return Response.ok().build();
    }

    @GET
    @Path("{idUser}/service/{idService}/picture")
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Picture getUserServicePicture(@PathParam("idUser") long idUser, @PathParam("idService") long idService) {
        return model.getServicePicture(model.getUser(idUser), idService);
    }

    @POST
    @Path("{idUser}/service/{idService}/picture")
    @Consumes(MediaTypeUTF8.APPLICATION_JSON)
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Response addUserServicePicture(@PathParam("idUser") long idUser, @PathParam("idService") long idService, Picture picture) {
        long idPicture = model.addServicePicture(model.getUserProposedService(model.getUser(idUser), idService), picture);
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(Long.toString(idPicture));

        return Response.created(uriBuilder.build()).entity(idPicture).build();
    }
}
