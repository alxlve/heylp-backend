package fr.lavie.alexis.heylp.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import fr.lavie.alexis.heylp.backend.model.enums.Gender;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

@XmlRootElement
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String username;
    private String password;
    private String email;
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Picture picture;
    private String firstName;
    private String lastName;
    private Gender gender;
    private Date birthday;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Address address;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "owner", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Collection<Phone> phones;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "suscriber")
    private Collection<Service> acceptedServices;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "owner", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Collection<Service> proposedServices;

    public User(String username, String password, String email, String firstName, String lastName, Gender gender, Date birthday) {
        this();

        this.username = username;
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthday = birthday;
        this.phones = new HashSet<Phone>();
        this.acceptedServices = new HashSet<Service>();
        this.proposedServices = new HashSet<Service>();
    }

    public User() {
        super();

        this.phones = new HashSet<Phone>();
        this.acceptedServices = new HashSet<Service>();
        this.proposedServices = new HashSet<Service>();
    }

    public void addPhone(Phone phone) {
        phone.setOwner(this);
        phones.add(phone);
    }

    public void addAcceptedService(Service service) {
        service.setSuscriber(this);
        acceptedServices.add(service);
    }

    public void addProposedService(Service service) {
        service.setOwner(this);
        proposedServices.add(service);
    }

    public void removePhone(Phone phone) {
        phones.remove(phone);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        address.setOwner(this);
        this.address = address;
    }

    @XmlTransient
    public Collection<Phone> getPhones() {
        return phones;
    }

    public void setPhones(Collection<Phone> phones) {
        this.phones = phones;
    }

    @XmlTransient
    public Collection<Service> getAcceptedServices() {
        return acceptedServices;
    }

    public void setAcceptedServices(Collection<Service> acceptedServices) {
        this.acceptedServices = acceptedServices;
    }

    @XmlTransient
    public Collection<Service> getProposedServices() {
        return proposedServices;
    }

    public void setProposedServices(Collection<Service> proposedServices) {
        this.proposedServices = proposedServices;
    }

    @JsonProperty("ownerNote")
    public Float[] getOwnerNote() {
        int count = 0;
        Float total = new Float(0.0);

        for (Service service : proposedServices) {
            Float note = service.getOwnerNote();
            if (note != null) {
                total += note;
                count++;
            }
        }

        if (count > 0) {
            return new Float[]{total / count, new Float(count)};
        }

        return null;
    }

    @JsonProperty("suscriberNote")
    public Float[] getSuscriberNote() {
        int count = 0;
        Float total = new Float(0.0);

        for (Service service : acceptedServices) {
            Float note = service.getSuscriberNote();
            if (note != null) {
                total += note;
                count++;
            }
        }

        if (count > 0) {
            return new Float[]{total / count, new Float(count)};
        }

        return null;
    }
}
