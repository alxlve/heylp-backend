package fr.lavie.alexis.heylp.backend.dao.jpa;

import fr.lavie.alexis.heylp.backend.dao.ServiceDao;
import fr.lavie.alexis.heylp.backend.model.Service;
import fr.lavie.alexis.heylp.backend.model.User;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceCategory;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceStatus;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Collection;

@Repository("serviceDao")
public class ServiceJpaDao extends AbstractJpaDao<Service> implements ServiceDao {

    public ServiceJpaDao(Class<Service> entityClass) {
        super(entityClass);
    }

    public ServiceJpaDao() {
        this(Service.class);
    }


    @Override
    public Collection<Service> getAllByOwner(User owner) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Service> criteriaQuery = criteriaBuilder.createQuery(Service.class);

        Root<Service> root = criteriaQuery.from(Service.class);

        criteriaQuery.where(criteriaBuilder.equal(root.<User>get("owner"), criteriaBuilder.parameter(User.class, "owner")));

        TypedQuery<Service> typedQuery = entityManager.createQuery(criteriaQuery);
        typedQuery.setParameter("owner", owner);

        return typedQuery.getResultList();
    }

    @Override
    public Collection<Service> getAllBySuscriberAndStatus(User suscriber, ServiceStatus serviceStatus) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Service> criteriaQuery = criteriaBuilder.createQuery(Service.class);

        Root<Service> root = criteriaQuery.from(Service.class);

        Predicate suscriberPredicate = criteriaBuilder.equal(root.<User>get("suscriber"), criteriaBuilder.parameter(User.class, "suscriber"));
        Predicate statusPredicate = criteriaBuilder.equal(root.<ServiceStatus>get("status"), criteriaBuilder.parameter(ServiceStatus.class, "status"));
        Predicate predicateAnd = criteriaBuilder.and(suscriberPredicate, statusPredicate);

        criteriaQuery.where(predicateAnd);

        TypedQuery<Service> typedQuery = entityManager.createQuery(criteriaQuery);
        typedQuery.setParameter("suscriber", suscriber);
        typedQuery.setParameter("status", serviceStatus);

        return typedQuery.getResultList();
    }

    @Override
    public Collection<Service> getAllByStatus(ServiceStatus serviceStatus) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Service> criteriaQuery = criteriaBuilder.createQuery(Service.class);

        Root<Service> root = criteriaQuery.from(Service.class);
//        criteriaQuery.where(criteriaBuilder.like(root.get("status"), ServiceStatus.AVAILABLE));
        criteriaQuery.where(criteriaBuilder.equal(root.<ServiceStatus>get("status"), criteriaBuilder.parameter(ServiceStatus.class, "status")));

        TypedQuery<Service> typedQuery = entityManager.createQuery(criteriaQuery);
        typedQuery.setParameter("status", serviceStatus);

//        return entityManager.createQuery(criteriaQuery).getResultList();
        return typedQuery.getResultList();
    }

    @Override
    public Collection<Service> getAllByStatusAndCategory(ServiceStatus serviceStatus, ServiceCategory serviceCategory) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Service> criteriaQuery = criteriaBuilder.createQuery(Service.class);

        Root<Service> root = criteriaQuery.from(Service.class);

        Predicate status = criteriaBuilder.equal(root.<ServiceStatus>get("status"), criteriaBuilder.parameter(ServiceStatus.class, "status"));
        Predicate category = criteriaBuilder.equal(root.<ServiceCategory>get("category"), criteriaBuilder.parameter(ServiceCategory.class, "category"));
        Predicate predicateAnd = criteriaBuilder.and(status, category);

        criteriaQuery.where(predicateAnd);

        TypedQuery<Service> typedQuery = entityManager.createQuery(criteriaQuery);
        typedQuery.setParameter("status", serviceStatus);
        typedQuery.setParameter("category", serviceCategory);

        return typedQuery.getResultList();
    }
}
