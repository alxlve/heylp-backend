package fr.lavie.alexis.heylp.backend.dao.jpa;

import fr.lavie.alexis.heylp.backend.dao.UserDao;
import fr.lavie.alexis.heylp.backend.model.User;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceCategory;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceStatus;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository("userDao")
public class UserJpaDao extends AbstractJpaDao<User> implements UserDao {

    public UserJpaDao(Class<User> entityClass) {
        super(entityClass);
    }

    public UserJpaDao() {
        this(User.class);
    }

    @Override
    public long login(String username, String password) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);

        Root<User> root = criteriaQuery.from(User.class);

        Predicate usernamePredicate = criteriaBuilder.equal(root.<ServiceStatus>get("username"), criteriaBuilder.parameter(String.class, "username"));
        Predicate passwordPredicate = criteriaBuilder.equal(root.<ServiceCategory>get("password"), criteriaBuilder.parameter(String.class, "password"));
        Predicate predicateAnd = criteriaBuilder.and(usernamePredicate, passwordPredicate);

        criteriaQuery.where(predicateAnd);

        TypedQuery<User> typedQuery = entityManager.createQuery(criteriaQuery);
        typedQuery.setParameter("username", username);
        typedQuery.setParameter("password", password);

        List<User> results = typedQuery.getResultList();
        if (!results.isEmpty()) {
            return results.get(0).getId();
        }

        return 0;
    }
}
