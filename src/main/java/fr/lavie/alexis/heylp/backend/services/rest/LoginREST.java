package fr.lavie.alexis.heylp.backend.services.rest;

import fr.lavie.alexis.heylp.backend.services.model.APIModel;
import fr.lavie.alexis.heylp.backend.services.rest.helper.MediaTypeUTF8;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("_login")
public class LoginREST {

    private static APIModel model;

    @Context
    UriInfo uriInfo;

    public LoginREST() {
        super();

        model = fr.lavie.alexis.heylp.backend.services.Application.getInstance().getModel();
    }

    @POST
    @Path("login")
    @Consumes(MediaTypeUTF8.APPLICATION_JSON)
    @Produces(MediaTypeUTF8.APPLICATION_JSON)
    public Response login(String jsonString) {
        JSONObject jsonObject = new JSONObject(jsonString);
        String username = jsonObject.getString("username");
        String password = jsonObject.getString("password");

        return Response.ok().entity(model.login(username, password)).build();
    }
}