package fr.lavie.alexis.heylp.backend.dao;

import fr.lavie.alexis.heylp.backend.model.Address;

public interface AddressDao extends AbstractDao<Address> {
}
