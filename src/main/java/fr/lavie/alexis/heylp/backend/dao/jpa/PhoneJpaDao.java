package fr.lavie.alexis.heylp.backend.dao.jpa;

import fr.lavie.alexis.heylp.backend.dao.PhoneDao;
import fr.lavie.alexis.heylp.backend.model.Phone;
import org.springframework.stereotype.Repository;

@Repository("phoneDao")
public class PhoneJpaDao extends AbstractJpaDao<Phone> implements PhoneDao {

    public PhoneJpaDao(Class<Phone> entityClass) {
        super(entityClass);
    }

    public PhoneJpaDao() {
        this(Phone.class);
    }
}
