package fr.lavie.alexis.heylp.backend.dao;

import fr.lavie.alexis.heylp.backend.model.Service;
import fr.lavie.alexis.heylp.backend.model.User;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceCategory;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceStatus;

import java.util.Collection;

public interface ServiceDao extends AbstractDao<Service> {

    Collection<Service> getAllByOwner(User owner);

    Collection<Service> getAllBySuscriberAndStatus(User suscriber, ServiceStatus serviceStatus);

    Collection<Service> getAllByStatus(ServiceStatus serviceStatus);

    Collection<Service> getAllByStatusAndCategory(ServiceStatus serviceStatus, ServiceCategory serviceCategory);
}
