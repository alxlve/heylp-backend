package fr.lavie.alexis.heylp.backend.dao.jpa;

import fr.lavie.alexis.heylp.backend.dao.AbstractDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.Collection;

public class AbstractJpaDao<T> implements AbstractDao<T> {

    @PersistenceContext
    protected EntityManager entityManager;

    private Class<T> classType;

    public AbstractJpaDao(Class<T> entityClass) {
        this.classType = entityClass;
    }

    @Override
    public T get(long id) {
        return entityManager.find(classType, id);
    }

    @Override
    public Collection<T> getAll() {
        CriteriaQuery<T> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(classType);
        criteriaQuery.select(criteriaQuery.from(classType));

        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public Collection<T> getRange(int[] range) {
        CriteriaQuery<T> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(classType);
        criteriaQuery.select(criteriaQuery.from(classType));
        javax.persistence.Query query = entityManager.createQuery(criteriaQuery);
        query.setMaxResults(range[1] - range[0]);
        query.setFirstResult(range[0]);

        return query.getResultList();
    }

    @Override
    public void add(T entity) {
        entityManager.persist(entity);
        entityManager.flush();
    }

    @Override
    public void save(T entity) {
        entityManager.merge(entity);
    }

    @Override
    public void delete(T entity) {
        entityManager.remove(entityManager.merge(entity));
    }

    @Override
    public int count() {
        CriteriaBuilder b = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteria = b.createQuery(Long.class);
        criteria.select(b.count(criteria.from(classType)));

        return entityManager.createQuery(criteria).getSingleResult().intValue();
    }
}
