package fr.lavie.alexis.heylp.backend.config;

public class Web {

    private static final String hostIp = "localhost";
    private static final String hostPort = "8080";
    private static final String restPattern = "rest";
    public static final String restUrl = "http://" + hostIp + ":" + hostPort + "/" + restPattern + "/";
}
