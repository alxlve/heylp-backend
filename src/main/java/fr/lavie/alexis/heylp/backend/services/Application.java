package fr.lavie.alexis.heylp.backend.services;

import fr.lavie.alexis.heylp.backend.services.model.APIModel;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {

    private static Application INSTANCE = null;
    private static APIModel model = null;

    private Application() {
        super();

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-config.xml");
        model = (APIModel) applicationContext.getBean("APIModel");
    }

    public static Application getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Application();
        }

        return INSTANCE;
    }

    public APIModel getModel() {
        return model;
    }
}
