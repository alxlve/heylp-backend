package fr.lavie.alexis.heylp.backend.services.rest.helper;

public class MediaTypeUTF8 {

    public static final String APPLICATION_JSON = javax.ws.rs.core.MediaType.APPLICATION_JSON + ";charset=utf-8";
}
