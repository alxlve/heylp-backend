package fr.lavie.alexis.heylp.backend.model.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ServiceCategory {

    @JsonProperty("Babysitting")
    BABYSITTING(1),
    @JsonProperty("Beauty")
    BEAUTY(2),
    @JsonProperty("Computer")
    COMPUTER(3),
    @JsonProperty("Delivery")
    DELIVERY(4),
    @JsonProperty("Do_it_yourself")
    DO_IT_YOURSELF(5),
    @JsonProperty("Events")
    EVENTS(6),
    @JsonProperty("Gardening")
    GARDENING(7),
    @JsonProperty("Homework_assistance")
    HOMEWORK_ASSISTANCE(8),
    @JsonProperty("Housekeeping")
    HOUSEKEEPING(9),
    @JsonProperty("Pets")
    PETS(10),
    @JsonProperty("Other")
    OTHER(11);

    private Integer value;

    ServiceCategory(Integer value) {
        this.value = value;
    }

    public static Integer a(String value) {
        switch (value) {
            case "Babysitting":
                return 1;
            case "Beauty":
                return 2;
            case "Computer":
                return 3;
            case "Delivery":
                return 4;
            case "Do_it_yourself":
                return 5;
            case "Events":
                return 6;
            case "Gardening":
                return 7;
            case "Homework_assistance":
                return 8;
            case "Housekeeping":
                return 9;
            case "Pets":
                return 10;
            case "Other":
                return 11;
        }
        return 0;
    }

    public Integer getValue() {
        return this.value;
    }
}
