package fr.lavie.alexis.heylp.backend.services.rest.run;

import fr.lavie.alexis.heylp.backend.model.Address;
import fr.lavie.alexis.heylp.backend.model.Service;
import fr.lavie.alexis.heylp.backend.model.User;
import fr.lavie.alexis.heylp.backend.model.enums.Gender;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceCategory;
import fr.lavie.alexis.heylp.backend.model.enums.ServiceStatus;
import fr.lavie.alexis.heylp.backend.services.model.APIModel;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Date;

@Path("run")
public class RunREST {

    private static APIModel model;

    @Context
    UriInfo uriInfo;

    public RunREST() {
        super();

        model = fr.lavie.alexis.heylp.backend.services.Application.getInstance().getModel();
    }

    @GET
    @Path("user")
    public Response runUser() {
        Address address1 = new Address("10", "rue de la Source", "Orléans", "45000", "France");
        User user1 = new User("a.l", "abc", "alexis.lv@mail.org", "Alexis", "Lavie", Gender.MALE, new Date());
        Address address2 = new Address("78", "rue de la Faculté", "Olivet", "45160", "France");
        User user2 = new User("jean-michel.moal", "java", "jeanmichel@bjr.fr", "Jean-Michel", "Moal", Gender.FEMALE, new Date());

        user1.setAddress(address1);
        user2.setAddress(address2);
        model.addUser(user1);
        model.addUser(user2);

        return Response.ok().build();
    }

    @GET
    @Path("service")
    public Response runService() {
        User user1 = new User();
        user1.setFirstName("Jean");
        User user2 = new User();
        user2.setFirstName("Luc");

        Address addressU = new Address("2", "rue de la Source", "Orléans", "45000", "France");
        Address address1 = new Address("10", "rue de la Source", "Orléans", "45000", "France");
        Service service1 = new Service(ServiceCategory.BABYSITTING, "Un projet de baby", "Description", 15, new Date(), new Date(), new Date());
        service1.setStatus(ServiceStatus.AVAILABLE);
        service1.setDescription("Blabla");
        Service service2 = new Service(ServiceCategory.EVENTS, "Un projet événementiel", "Description", 15, new Date(), new Date(), new Date());
        service2.setStatus(ServiceStatus.AVAILABLE);

        user1.setAddress(addressU);
        model.addUser(user1);
        model.addUser(user2);

        service1.setAddress(address1);
        model.addUserProposedService(user1, service1);
        model.addUserProposedService(user2, service2);

        return Response.ok().build();
    }
}
