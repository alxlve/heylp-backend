package fr.lavie.alexis.heylp.backend.model.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ServiceStatus {

    @JsonProperty("Available")
    AVAILABLE(1),
    @JsonProperty("Affected")
    AFFECTED(2),
    @JsonProperty("Accomplished")
    ACCOMPLISHED(3),
    @JsonProperty("Canceled")
    CANCELED(4);

    private Integer value;

    ServiceStatus(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }
}
