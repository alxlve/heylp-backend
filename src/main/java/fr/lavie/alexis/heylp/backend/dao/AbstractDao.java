package fr.lavie.alexis.heylp.backend.dao;

import java.util.Collection;

public interface AbstractDao<T> {

    T get(long id);

    Collection<T> getAll();

    Collection<T> getRange(int[] range);

    void add(T entity);

    void save(T entity);

    void delete(T entity);

    int count();
}
