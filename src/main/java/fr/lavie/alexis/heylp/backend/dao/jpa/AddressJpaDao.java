package fr.lavie.alexis.heylp.backend.dao.jpa;

import fr.lavie.alexis.heylp.backend.dao.AddressDao;
import fr.lavie.alexis.heylp.backend.model.Address;
import org.springframework.stereotype.Repository;

@Repository("addressDao")
public class AddressJpaDao extends AbstractJpaDao<Address> implements AddressDao {

    public AddressJpaDao(Class<Address> entityClass) {
        super(entityClass);
    }

    public AddressJpaDao() {
        this(Address.class);
    }
}
