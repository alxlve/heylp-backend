package fr.lavie.alexis.heylp.backend.model.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum PhoneType {

    @JsonProperty("Home")
    HOME(1),
    @JsonProperty("Work")
    WORK(2),
    @JsonProperty("Fax")
    FAX(3),
    @JsonProperty("Mobile")
    MOBILE(4);

    private Integer value;

    PhoneType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }
}
